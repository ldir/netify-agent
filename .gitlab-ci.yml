# -------------------------------------------------------------------------
# Definitions
# -------------------------------------------------------------------------

stages:
  - build
  - test

# variables:
#  CI_DEBUG_TRACE: "true"

# Incude templates
include:
  - '/.gitlab-ci/build-templates.yml'
  - '/.gitlab-ci/test-templates.yml'

# -------------------------------------------------------------------------
# Build stage
# -------------------------------------------------------------------------

# Debian OS Targets
#------------------

.debian 10 build:
  extends: .debian_build
  tags:
    - docker
  variables:
    OS_IMAGE: "debian:10"
    OS_NAME: "debian"
    OS_VERSION: "10"
    OS_ARCH: "x86_64"

.debian 11 build:
  extends: .debian11_build
  tags:
    - debian11

# RPM OS Targets
#---------------

centos 7 build:
  extends: .rpmos_build
  tags:
    - docker
  variables:
    RPMOS_IMAGE: "registry.gitlab.com/egloo.ca/os-manager/builder-image:os7"
    RPMOS_OS: "centos"
    RPMOS_VERSION: "7"
    RPMOS_ARCH: "x86_64"

almalinux 8 build:
  extends: .rpmos_build
  tags:
    - docker
  variables:
    RPMOS_IMAGE: "registry.gitlab.com/egloo.ca/os-manager/builder-image:os8"
    RPMOS_OS: "almalinux"
    RPMOS_VERSION: "8"
    RPMOS_ARCH: "x86_64"

# FreeBSD OS Targets
#-------------------

freebsd 11.2 build:
  extends: .xbsd_build
  tags:
    - freebsd112
  only:
    - web
  variables:
    OS_BASENAME: "freebsd"
    OS_VERSION: "11"
    OS_RELEASE: "2"
    OS_ARCH: "x86_64"
    OS_MAKE_TARGET: "deploy-freebsd-11x"
    OS_CONFIGURE_FLAGS: ""
    OS_PKG_EXTENSION: "txz"

# freebsd 11.3 build:
#   extends: .xbsd_build
#   tags:
#     - freebsd113
#   variables:
#     OS_BASENAME: "freebsd"
#     OS_VERSION: "11"
#     OS_RELEASE: "3"
#     OS_ARCH: "x86_64"
#     OS_MAKE_TARGET: "deploy-freebsd-11x"
#     OS_CONFIGURE_FLAGS: ""
#     OS_PKG_EXTENSION: "txz"

freebsd 12.1 build:
  extends: .xbsd_build
  tags:
    - freebsd121
  only:
    - web
  variables:
    OS_BASENAME: "freebsd"
    OS_VERSION: "12"
    OS_RELEASE: "1"
    OS_ARCH: "x86_64"
    OS_MAKE_TARGET: "deploy-freebsd-12x"
    OS_CONFIGURE_FLAGS: "--disable-libtcmalloc"
    OS_PKG_EXTENSION: "txz"

freebsd 12.3 build:
  extends: .xbsd_build
  tags:
    - freebsd123
  only:
    - web
  variables:
    OS_BASENAME: "freebsd"
    OS_VERSION: "12"
    OS_RELEASE: "3"
    OS_ARCH: "x86_64"
    OS_MAKE_TARGET: "deploy-freebsd-12x"
    OS_CONFIGURE_FLAGS: ""
    OS_PKG_EXTENSION: "pkg"

# -------------------------------------------------------------------------
# Test stage
# -------------------------------------------------------------------------

# CentOS
#-------

centos 7 test:
  extends: .rpmos_test
  tags:
    - docker
  variables:
    RPMOS_OS: "centos"
    RPMOS_VERSION: "7"
    RPMOS_IMAGE: "centos:7"
    RPMOS_ARCH: "x86_64"
    RPMOS_UUID: "NE-CE-07-X1"
  needs: ["centos 7 build"]

almalinux 8 test:
  extends: .rpmos_test
  tags:
    - docker
  variables:
    RPMOS_OS: "almalinux"
    RPMOS_VERSION: "8"
    RPMOS_IMAGE: "almalinux:8"
    RPMOS_ARCH: "x86_64"
    RPMOS_UUID: "NE-CE-08-X1"
  needs: ["almalinux 8 build"]

# OPNsense/pfSense
#-----------------

opnsense 19.7 test:
  extends: .xsense_test
  tags:
    - opnsense197
  only:
    - web
  variables:
    AGENT_UUID: "NE-PN-19-XC"
    FREEBSD_VERSION: "11"
    FREEBSD_RELEASE: "2"
    FREEBSD_LIBS_RELEASE: "3"
    FREEBSD_PKG_EXTENSION: "txz"
  before_script:
    - ntpdate -u time.nist.gov
    - pkg add http://pkg.freebsd.org/freebsd:$FREEBSD_VERSION:x86:64/release_$FREEBSD_LIBS_RELEASE/All/libunwind-20170615.txz
    - pkg add http://pkg.freebsd.org/freebsd:$FREEBSD_VERSION:x86:64/release_$FREEBSD_LIBS_RELEASE/All/google-perftools-2.7.txz
  needs: ["freebsd 11.2 build"]

opnsense 20.1 test:
  extends: .xsense_test
  tags:
    - opnsense201
  only:
    - web
  variables:
    AGENT_UUID: "NE-PN-20-XC"
    FREEBSD_VERSION: "11"
    FREEBSD_RELEASE: "2"
    FREEBSD_LIBS_RELEASE: "3"
    FREEBSD_PKG_EXTENSION: "txz"
  before_script:
    - ntpdate -u time.nist.gov
    - pkg add http://pkg.freebsd.org/freebsd:$FREEBSD_VERSION:x86:64/release_$FREEBSD_LIBS_RELEASE/All/libunwind-20170615.txz
    - pkg add http://pkg.freebsd.org/freebsd:$FREEBSD_VERSION:x86:64/release_$FREEBSD_LIBS_RELEASE/All/google-perftools-2.7.txz
  needs: ["freebsd 11.2 build"]

opnsense 20.7 test:
  extends: .xsense_test
  tags:
   - opnsense207
  only:
    - web
  variables:
    AGENT_UUID: "NE-PN-27-XC"
    FREEBSD_VERSION: "12"
    FREEBSD_RELEASE: "1"
    FREEBSD_LIBS_RELEASE: "1"
    FREEBSD_PKG_EXTENSION: "txz"
  before_script:
    - ntpdate -u time.nist.gov
  needs: ["freebsd 12.1 build"]

pfsense 2.4.5 test:
  extends: .xsense_test
  tags:
    - pfsense245
  only:
    - web
  variables:
    AGENT_UUID: "NE-PF-24-XC"
    FREEBSD_VERSION: "11"
    FREEBSD_RELEASE: "2"
    FREEBSD_LIBS_RELEASE: "3"
    FREEBSD_PKG_EXTENSION: "txz"
  before_script:
    - ntpdate -u time.nist.gov
    - pkg add http://pkg.freebsd.org/freebsd:$FREEBSD_VERSION:x86:64/release_$FREEBSD_LIBS_RELEASE/All/libunwind-20170615.txz
    - pkg add http://pkg.freebsd.org/freebsd:$FREEBSD_VERSION:x86:64/release_$FREEBSD_LIBS_RELEASE/All/google-perftools-2.7.txz
  needs: ["freebsd 11.2 build"]

pfsense 2.6.0 test:
  extends: .xsense_test
  tags:
    - pfsense260
  only:
    - web
  variables:
    AGENT_UUID: "NE-PF-26-XC"
    FREEBSD_VERSION: "12"
    FREEBSD_RELEASE: "3"
    FREEBSD_PKG_EXTENSION: "pkg"
  needs: ["freebsd 12.3 build"]
